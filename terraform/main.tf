provider "aws" {
  access_key                  = "temp"
  region                      = "eu-west-1"
  secret_key                  = "temp"
  skip_credentials_validation = true
  skip_metadata_api_check     = true
  skip_requesting_account_id  = true
  s3_force_path_style = true

  endpoints {
    kinesis        = "http://localstack:4566"
    lambda         = "http://localstack:4566"
	sns         = "http://localstack:4566"
	ssm         = "http://localstack:4566"
	sqs         = "http://localstack:4566"
  iam         = "http://localstack:4566"
  }
}






